# EMAIL SERVICE

Project is build on java 17 SDK.

Spring Boot, MVC, Data JPA is used.

Mock MVC for integration tests.

You have to have maven installed for running maven commands.

# SETUP

Using git repository:

1. With ssh:

Copy SSH repository link:

![Screenshot](src/main/resources/images/ssh.png)


    git@gitlab.com:marcin92d/email-service.git

Open terminal in target directory:

example:

    ~/Users/User/IdeaProjects/project

Open terminal in project directory and run command:

    % git clone git@gitlab.com:marcin92d/email-service.git

2. With zip:

Download project ZIP file from repository:

![Screenshot](src/main/resources/images/zip.png)

Open zip file and copy project repository to target directory.

Now you can open project in you favourite IDE.

# CONFIGURATION

After opening project, you will have to configure application.properties:

Default H2 db:

![Screenshot](src/main/resources/images/h2.png)

Example MySQL db:

![Screenshot](src/main/resources/images/mysql.png)

Example gmail smtp:

![Screenshot](src/main/resources/images/smtp.png)

# RUN APPLICATION

1. Go to application root folder:
2. Build jar file:

Open terminal and run command:

    % mvn clean install

This will create target directory in your root directory.

To enter target folder command:

    % cd target

Maven should create jar file for application:

    email-service-0.0.1-SNAPSHOT.jar

3. Run application

To start application while being in /target directory run:

    % java -jar email-service-0.0.1-SNAPSHOT.jar

# API

It is a simple microservice that allows you to send text emails and perform CRUD operations on users.
It has a built-in H2 database containing users' email addresses. The service makes it possible to
send e-mail messages to all user addresses at once.

# Creating email

example:

    localhost:8080/api/email/create
    body:
        {
            "id": "1",
            "email": "example@email.com"
        }

POST request that saves User with id 1 and email address 'example@email.com' to db.

# Getting by email

example:

    localhost:8080/api/email/get/example@email.com

GET request that responds with Json format of User with email 'example@email.com'.

# Updating email

example:

    localhost:8080/api/email/example@email.com
    body:
        {
            "email": "update@email.com"
        }

PUT request that changes 'example@email.com' to 'update@email.com'

# Deleting email

example:

    localhost:8080/users/delete/example@email.com

DELETE request that deletes email 'example@email.com' from db.

# Sending emails

example:

    localhost:8080/mail/send
    body:
        {
            "from": "sender",
            "subject" : "test",
            "content": "message"
        }

POST request that sends email to all users emails in db.

# LOGGING

Application uses logback.xml to save logs in email-service/logs directory.

Contains of two separate log files:

1. general.log - saves all the logs of app runtime.
2. requests.log - saves only request logs.