package pl.dolega.emailservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("spring.mail")
public class MailConfiguration {

    private String host;
    private Integer port;
    private String username;
    private String password;

}
