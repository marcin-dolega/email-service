package pl.dolega.emailservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dolega.emailservice.domain.Mail;
import pl.dolega.emailservice.dto.MailDto;
import pl.dolega.emailservice.facade.Facade;

@RequestMapping("/mail")
@RestController
public class MailController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    Facade facade;

    @PostMapping("/send")
    public ResponseEntity<MailDto> SendMail(@RequestBody Mail mail) {
        facade.sendEmail(mail);
        logger.info("Email sent to emails in db");
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
