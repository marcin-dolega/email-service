package pl.dolega.emailservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dolega.emailservice.dto.UserDto;
import pl.dolega.emailservice.facade.Facade;

@RequestMapping("/api/email")
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private Facade facade;

    @PostMapping(value = "/create", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDto> save(@RequestBody UserDto dto) {
        facade.saveUser(dto);
        logger.info("User with email: + " + dto + " created");
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping("/get/{email}")
    public ResponseEntity<UserDto> getByEmail(@PathVariable String email) {
        try {
            UserDto dto = facade.getUserByEmail(email);
            logger.info("Email fetched: " + email);
            return new ResponseEntity<>(dto, HttpStatus.FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update/{currentEmail}")
    public ResponseEntity<UserDto> update(@PathVariable String currentEmail, @RequestBody UserDto dto) {
        facade.updateUser(currentEmail, dto);
        logger.info("Email: + " + dto.getEmail() + " updated to: " + currentEmail);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{email}")
    public ResponseEntity<Long> deleteById(@PathVariable String email) {
        facade.deleteUserByEmail(email);
        logger.info("User with email: + " + email + " deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
