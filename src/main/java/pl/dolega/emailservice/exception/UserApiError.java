package pl.dolega.emailservice.exception;

public class UserApiError extends RuntimeException {
    public UserApiError(String message) {
        super(message);
    }
}
