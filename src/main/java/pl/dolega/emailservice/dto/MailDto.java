package pl.dolega.emailservice.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailDto {

    @NotBlank
    private String mailFrom;
    @NotBlank
    private String mailSubject;
    @NotBlank
    private String mailContent;

}
