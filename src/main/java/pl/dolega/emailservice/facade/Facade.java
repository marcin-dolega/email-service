package pl.dolega.emailservice.facade;

import pl.dolega.emailservice.domain.Mail;
import pl.dolega.emailservice.dto.UserDto;

import java.util.List;

public interface Facade {

    void saveUser(UserDto dto);
    UserDto getUserByEmail(String email);
    void updateUser(String currentEmail, UserDto dto);
    void deleteUserByEmail(String email);

    void sendEmail(Mail mail);



}
