package pl.dolega.emailservice.facade.impl;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dolega.emailservice.domain.Mail;
import pl.dolega.emailservice.domain.User;
import pl.dolega.emailservice.dto.UserDto;
import pl.dolega.emailservice.facade.Facade;
import pl.dolega.emailservice.service.MailService;
import pl.dolega.emailservice.service.UserService;

@Component
public class FacadeImpl implements Facade {

    @Autowired
    private ModelMapper mapper;

    private MailService mailService;
    private UserService userService;


    public FacadeImpl(MailService mailService, UserService userService) {
        this.mailService = mailService;
        this.userService = userService;
    }

    @Override
    public void saveUser(UserDto dto) {
        userService.saveUser(mapper.map(dto, User.class));
    }

    @Override
    public UserDto getUserByEmail(String email) {
        return mapper.map(userService.getUserByEmail(email), UserDto.class);
    }

    @Override
    public void updateUser(String currentEmail, UserDto dto) {
        userService.updateEmail(currentEmail, mapper.map(dto, User.class));
    }

    @Override
    public void deleteUserByEmail(String email) {
        userService.deleteUserByEmail(email);
    }

    @Override
    public void sendEmail(Mail mail) {
        mailService.sendEmail(mail);
    }

}
