package pl.dolega.emailservice.service;

import pl.dolega.emailservice.domain.User;

import java.util.List;

public interface UserService {

    User saveUser(User user);
    User getUserByEmail(String email);
    void updateEmail(String currentEmail, User user);
    void deleteUserByEmail(String email);

    List<String> getEmails();

}
