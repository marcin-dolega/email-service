package pl.dolega.emailservice.service.impl;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pl.dolega.emailservice.domain.Mail;
import pl.dolega.emailservice.service.MailService;
import pl.dolega.emailservice.service.UserService;

@Service
public class MailServiceImpl implements MailService {

    private final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserService userService;

    @Override
    public void sendEmail(Mail mail) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        userService.getEmails()
                .forEach(email -> {
                    try {
                        MimeMessageHelper mmh = new MimeMessageHelper(mimeMessage, true);
                        mmh.setSubject(mail.getSubject());
                        mmh.setSubject(mail.getSubject());
                        mmh.setFrom("no_replay");
                        mmh.setTo(email);
                        mmh.setText(mail.getContent());
                        javaMailSender.send(mmh.getMimeMessage());
                        logger.info("Email sent to " + email);
                    } catch (MessagingException e) {
                        logger.info("Email not sent");
                        e.printStackTrace();
                    }
                }
        );
    }
}
