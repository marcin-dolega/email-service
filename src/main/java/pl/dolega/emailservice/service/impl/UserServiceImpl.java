package pl.dolega.emailservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dolega.emailservice.exception.UserApiError;
import pl.dolega.emailservice.domain.User;
import pl.dolega.emailservice.repository.UserRepository;
import pl.dolega.emailservice.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public User saveUser(User user) {
        if (userRepository.getUserByEmail(user.getEmail()).isPresent()) {
            logger.error("Email " + user.getEmail() + " already in database");
            throw new UserApiError("Email already in database");
        }
        var saved = userRepository.save(user);
        logger.info(user + " saved");
        return saved;
    }

    @Override
    public User getUserByEmail(String email) {
        for (User user : userRepository.findAll()) {
            if (user.getEmail().equals(email)) {
                logger.info(email + " found");
                return user;
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public void updateEmail(String currentEmail, User user) {
        var fetched = userRepository.getUserByEmail(currentEmail);

        if(fetched.isPresent()) {
            User fetchedUser = fetched.get();
            fetchedUser.setEmail(user.getEmail());
            userRepository.save(fetchedUser);
            logger.info(user + " updated to " + fetched);
        } else {
            throw new UserApiError("Invalid email.");
        }
    }

    @Override
    public void deleteUserByEmail(String email) {
        if (userRepository.findById(getUserByEmail(email).getId()).isPresent()) {
            userRepository.deleteById(getUserByEmail(email).getId());
            logger.info(email + " deleted");
        } else {
            logger.error("Email " + email + " not found in database");
        }
    }

    @Override
    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        userRepository.findAll().forEach(u -> emails.add(u.getEmail()));
        return emails;
    }
}
