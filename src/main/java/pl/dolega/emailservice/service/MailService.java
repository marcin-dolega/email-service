package pl.dolega.emailservice.service;

import pl.dolega.emailservice.domain.Mail;

public interface MailService {

    void sendEmail(Mail mail);

}
