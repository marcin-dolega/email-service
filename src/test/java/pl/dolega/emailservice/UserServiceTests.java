package pl.dolega.emailservice;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import pl.dolega.emailservice.domain.User;
import pl.dolega.emailservice.repository.UserRepository;
import pl.dolega.emailservice.service.UserService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@SpringBootTest
class UserServiceTests {

    @Autowired
    UserService userService;

    User user;
    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void createUser() {
        user = User.builder()
                .id(1)
                .email("test@email.com")
                .build();
    }

    @AfterEach
    void emptyData() {
        userRepository.deleteAll();
    }

    @Test
    void saveUser() {
        var result = userService.saveUser(user);
        assertNotNull(result);
        assertEquals(user, result);
    }

    @Test
    void getUserByEmail() {
        var saved = userService.saveUser(user);
        var result = userService.getUserByEmail(saved.getEmail());
        assertNotNull(result);
    }

    @Test
    void updateEmail() {
        String currentEmail = user.getEmail();
        User update = User.builder().id(user.getId()).email("new@email.com").build();
        userService.saveUser(user);
        userService.updateEmail(currentEmail, update);
        var result = userService.getUserByEmail("new@email.com");
        assertNotNull(result);
    }

    @Test
    void deleteUserByEmail() {
        userService.saveUser(user);
        userService.deleteUserByEmail(user.getEmail());
        assertEquals(0, userService.getEmails().size());
    }

    @Test
    void getEmailsTest() {
        userService.saveUser(user);
        var result = userService.getEmails();
        assertEquals(1, result.size());
        assertEquals("test@email.com", result.get(0));
    }
}