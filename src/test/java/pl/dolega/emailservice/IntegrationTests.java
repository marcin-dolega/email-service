package pl.dolega.emailservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.dolega.emailservice.domain.User;
import pl.dolega.emailservice.dto.UserDto;
import pl.dolega.emailservice.repository.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTests {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    ModelMapper mapper;

    private UserDto userDto;

    @BeforeEach
    void createUser() {
        userDto = UserDto.builder()
                .id(1)
                .email("test@email.com")
                .build();
    }

    @AfterEach
    void emptyData() {
        userRepository.deleteAll();
    }

    @Test
    public void createEmailTest() throws Exception {
        this.mockMvc.perform(
                        post("/api/email/create")
                                .content(objectMapper.writeValueAsString(userDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(status().isCreated());
    }

    @Test
    public void getUserByEmailTest() throws Exception {
        userRepository.save(mapper.map(userDto, User.class));

        this.mockMvc.perform(
                        get("/api/email/get/{email}", "test@email.com")
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("test@email.com"));
    }

    @Test
    public void updateEmailTest() throws Exception {
        userRepository.save(mapper.map(userDto, User.class));
        this.mockMvc.perform(
                put("/api/email/update/" + userDto.getEmail())
                        .content(objectMapper.writeValueAsString(UserDto.builder().email("update@email.com").build()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() throws Exception {
        userRepository.save(mapper.map(userDto, User.class));

        this.mockMvc.perform(delete("/api/email/delete/{email}", userDto.getEmail()))
                .andExpect(status().isOk());
    }

}
